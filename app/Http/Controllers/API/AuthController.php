<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class AuthController extends Controller
{
    //
    public function login(Request $request) {
    	$user = User::where('username', $request->username)->first();
    	if(is_object($user)) {
    		if(Hash::check($request->password, $user->password)) {
                $credentials = [
                    'username' => $request->username,
                    'password' => $request->password,
                ];

                $jwtController = new JWTController();
                $token = $jwtController->encrypt($credentials);

                $data = [
                    'user' => $user,
                    'token' => $token
                ];
                $response = [
                    'status' => 'success',
                    'message' => 'Welcome ' . $user->first_name . ' ' . $user->last_name,
                    'data' => $data,
                ];
    		}
    		else {
    			$response = [
    				'status' => 'failed',
    				'message' => 'Access Denied!'
    			];
    		}
    	}
    	else {
    		$response = [
    			'status' => 'failed',
    			'message' => 'Access Denied'
    		];
    	}

        return response()->json($response);
    }
}

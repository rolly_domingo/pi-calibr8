<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Firebase\JWT\JWT;

class JWTController extends Controller
{
    //
	protected $key = "calibr8_systems_inc";
    public function encrypt($data) {
    	$token = array(
		    "iss" => "http://rolly.domingo.com",
		    "aud" => "http://rolly.domingo.com",
		    "iat" => 1356999524,
		    "nbf" => 1357000000,
		    "data" => $data);

		$jwt = JWT::encode($token, $this->key);

		return $jwt;
    }

    public function decrypt($jwt) {
    	$decoded_jwt = JWT::decode($jwt, $key, array('HS256'));

    	return $decoded_jwt;
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class PIController extends Controller
{
    //

    protected function curlGet($path) {
    	$pi_server = env('PI_SERVER');
    	$pi_api = env('PI_API');
    	$username = env('PI_USERNAME');
    	$password = env('PI_PASSWORD');

    	$url = $pi_api . $path;
    	$ch = curl_init();  
 
    	curl_setopt($ch,CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, false);  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . base64_encode($username . ':' . $password), ]);
	 
	    $output=curl_exec($ch);
	 
	    curl_close($ch);

	    return json_decode($output, true);
    }

    protected function curlPost($path, $data) {
    	$pi_server = env('PI_SERVER');
    	$pi_api = env('PI_API');
    	$username = env('PI_USERNAME');
    	$password = env('PI_PASSWORD');

    	$url = $pi_api . $path;

    	$curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_HEADER, true); 
		curl_setopt($curl, CURLOPT_NOBODY, true);   
    	curl_setopt($curl, CURLOPT_POST, 1);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"Content-Type:application/json",
			"Authorization:Basic " . base64_encode($username . ":" . $password),   
			)                                                                   
		);   
		// Make the REST call, returning the result
		$response = curl_exec($curl);
		$response_info = explode(PHP_EOL, $response);
		$response_header = explode(" ", $response_info[0]);
		$http_code = $response_header[1];
		$http_description = $response_header[2];

		if($http_code >= 300) {
			$output = [
				'status' => 'failed',
				'message' => json_decode(end($response_info), true)
			];
		}
		else {
			$output = [
				'status' => 'success',
				'message' => 'Submitted'
			];
		}
		

		return $output;
    }

    protected function curlDelete($path) {
        $pi_server = env('PI_SERVER');
        $pi_api = env('PI_API');
        $username = env('PI_USERNAME');
        $password = env('PI_PASSWORD');

        $url = $pi_api . $path;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_HEADER, true); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization:Basic " . base64_encode($username . ":" . $password),   
            )                                                                   
        );   
        
        $response = curl_exec($curl);
        $response_info = explode(PHP_EOL, $response);
        $response_header = explode(" ", $response_info[0]);
        $http_code = $response_header[1];
        $http_description = $response_header[2];

        if($http_code >= 300) {
            $output = [
                'status' => 'failed',
                'message' => json_decode(end($response_info), true)
            ];
        }
        else {
            $output = [
                'status' => 'success',
                'message' => 'Submitted'
            ];
        }
        

        return $output;
    }

    public function getAssetServers() {
    	$path = "/assetservers";
    	$servers = $this->curlget($path);
    	$server_arr = [];
    	foreach($servers['Items'] as $k => $server) {
    		$server_arr[$k]['WebId'] = $server['WebId'];
    		$server_arr[$k]['Id'] = $server['Id'];
    		$server_arr[$k]['Name'] = $server['Name'];
    		$server_arr[$k]['Description'] = $server['Description'];
    		$server_arr[$k]['Path'] = $server['Path'];
    	}

    	$response = [
    		'status' => 'success',
    		'message' => 'Asset Servers has been loaded',
    		'data' => $server_arr
    	];

    	return response()->json($response);
    }

    public function getAssetDatabases($webid) {
    	$path = '/assetservers/'. $webid .'/assetdatabases';
    	$databases = $this->curlGet($path);

    	$databases_arr = [];
    	foreach($databases['Items'] as $k => $d) {
    		$databases_arr[$k]['WebId'] = $d['WebId'];
    		$databases_arr[$k]['Id'] = $d['Id'];
    		$databases_arr[$k]['Name'] = $d['Name'];
    		$databases_arr[$k]['Description'] = $d['Description'];
    		$databases_arr[$k]['Path'] = $d['Path'];
    	}

    	$response = [
    		'status' => 'success',
    		'message' => 'Asset Servers has been loaded',
    		'data' => $databases_arr
    	];

    	return response()->json($response);
    }

    public function getAssetElements($webid) {
    	$path = '/assetdatabases/' . $webid . '/elements';
    	$elements = $this->curlGet($path);
    	$elements_arr = [];
    	foreach($elements['Items'] as $k => $element) {
			$elements_arr[$k]['WebId'] = $element['WebId'];    		
			$elements_arr[$k]['Id'] = $element['Id'];    		
			$elements_arr[$k]['Name'] = $element['Name'];    		
			$elements_arr[$k]['Description'] = $element['Description'];
			$elements_arr[$k]['Path'] = $element['Path'];
			$elements_arr[$k]['HasChildren'] = $element['HasChildren'];
    	}

    	$response = [
    		'status' => 'success',
    		'message' => 'Elements has been loaded',
    		'data' => $elements_arr
    	];

    	return response()->json($response);
    }

    public function addParentElement(Request $request) {
    	$path = '/assetdatabases/' . $request->WebId . '/elements';

    	$data = array(
			"Name" => $request->name,
			"Description" => $request->description,
			"TemplateName" => '',
			"CategoryNames" => [],
			"ExtendedProperties" => ''
		);

		$output = $this->curlPost($path, $data);

		if($output['status'] == 'success') {
			$response = [
				'status' => 'success',
				'message' => 'Element has been added',
			];	
		}
		else {
			$response = [
				'status' => 'failed',
				'message' => 'Something went wrong',
				'errors' => $output['message']['Errors']
			];	
		}
		

		return response()->json($response);
    }

    public function addChildElement(Request $request) {
    	$path = '/elements/' . $request->WebId . '/elements';

    	$data = array(
			"Name" => $request->name,
			"Description" => $request->description,
			"TemplateName" => '',
			"CategoryNames" => [],
			"ExtendedProperties" => ''
		);
    	$output = $this->curlPost($path, $data);

		$response = [
			'status' => 'success',
			'message' => 'Element has been added',
			'data' => $output,
		];

		if($output['status'] == 'success') {
			$response = [
				'status' => 'success',
				'message' => 'Element has been added',
			];	
		}
		else {
			$response = [
				'status' => 'failed',
				'message' => 'Something went wrong',
				'errors' => $output['message']['Errors']
			];	
		}
		

		return response()->json($response);
    }

    public function getChildElements($webid) {
    	$path = '/elements/' . $webid . '/elements';
    	$elements = $this->curlGet($path);
    	$elements_arr = [];
    	foreach($elements['Items'] as $k => $element) {
			$elements_arr[$k]['WebId'] = $element['WebId'];    		
			$elements_arr[$k]['Id'] = $element['Id'];    		
			$elements_arr[$k]['Name'] = $element['Name'];    		
			$elements_arr[$k]['Description'] = $element['Description'];
			$elements_arr[$k]['Path'] = $element['Path'];
			$elements_arr[$k]['HasChildren'] = $element['HasChildren'];
    	}

    	$response = [
    		'status' => 'success',
    		'message' => 'Elements has been loaded',
    		'data' => $elements_arr
    	];

    	return response()->json($response);
    }

    public function getAttributes($webid) {
    	$path = '/elements/' . $webid . '/attributes';
    	$attributes = $this->curlGet($path);
    	$attributes_arr = [];
    	foreach($attributes['Items'] as $k => $attribute) {
    		$attributes_arr[$k]['WebId'] = $attribute['WebId'];
    		$attributes_arr[$k]['Id'] = $attribute['Id'];
    		$attributes_arr[$k]['Name'] = $attribute['Name'];
    		$attributes_arr[$k]['Description'] = $attribute['Description'];
    		$attributes_arr[$k]['Path'] = $attribute['Path'];
    		$attributes_arr[$k]['Type'] = $attribute['Type'];
    		$attributes_arr[$k]['TypeQualifier'] = $attribute['TypeQualifier'];
    		$attributes_arr[$k]['DefaultUnitsName'] = $attribute['DefaultUnitsName'];
    		$attributes_arr[$k]['DataReferencePlugIn'] = $attribute['DataReferencePlugIn'];
    	}

    	$response = [
    		'status' => 'success',
    		'message' => 'Attributes has been loaded',
    		'data' => $attributes_arr
    	];

    	return response()->json($response);

    }

    public function getUOM($webid) {
        $path = '/assetservers/' . $webid . '/unitclasses';
        $units = $this->curlGet($path);
        $units_arr = [];

        foreach($units['Items'] as $k => $u) {
            $units_arr[$k]['WebId'] = $u['WebId'];
            $units_arr[$k]['Id'] = $u['Id'];
            $units_arr[$k]['Name'] = $u['Name'];
            $units_arr[$k]['Description'] = $u['Description'];
            $units_arr[$k]['CanonicalUnitName'] = $u['CanonicalUnitName'];
            $units_arr[$k]['CanonicalUnitAbbreviation'] = $u['CanonicalUnitAbbreviation'];
        }

       $response = [
            'status' => 'success',
            'message' => 'Units has been loaded',
            'data' => $units_arr
        ];

        return response()->json($response);

    }

    public function getChildUOM($webid) {
        $path = '/unitclasses/' . $webid . '/units';
        $units = $this->curlGet($path);
        $unit_arr = [];

        foreach($units['Items'] as $k => $u) {
            $units_arr[$k]['WebId'] = $u['WebId'];
            $units_arr[$k]['Id'] = $u['Id'];
            $units_arr[$k]['Name'] = $u['Name'];
            $units_arr[$k]['Abbreviation'] = $u['Abbreviation'];
            $units_arr[$k]['Description'] = $u['Description'];
        }

       $response = [
            'status' => 'success',
            'message' => 'Units has been loaded',
            'data' => $units_arr
        ];

        return response()->json($response);
    }

    public function getAttributeCategories($webid) {
        $path = '/assetdatabases/' . $webid . '/attributecategories';
        $categories = $this->curlGet($path);
        
        $response = [
            'status' => 'success',
            'message' => 'Categories has been loaded',
            'data' => $categories['Items']
        ];

        return response()->json($response);

    }

    public function createAttribute($webid, Request $request) {
        $path = '/elements/' . $webid . '/attributes';
        $submit = $this->curlPost($path, $request->except(['token']));

        if($submit['status'] == 'success') {
            $response = [
                'status' => 'success',
                'message' => 'Attribute has been created',
                'data' => []
            ];
        }
        else {
            $response = [
                'status' => 'failed',
                'message' => 'Please check your inputs',
                'errors' => $submit['message']['Errors']
            ];
        }

        return response()->json($response);
    }

    public function deleteAttribute($webid) {
        // $path = '/attributes/' . $webid;
        // $delete = $this->curlDelete($path);

        // if($delete['status'] == 'success') {
        //     $response = [
        //         'status' => 'success',
        //         'message' => 'Attribute has been deleted.'
        //     ];
        // }
        // else {
        //     $response = [
        //         'status' => 'failed',
        //         'message' => $delete['message']['Errors']
        //     ];
        // }
        $response = [
            'status' => 'failed',
            'message' => ['Delete Feature is currently disabled for security purposes. ']
        ];

        return response()->json($response);
    }

    public function deleteElement($webid) {
        // $path = '/elements/' . $webid;
        // $delete = $this->curlDelete($path);

        // if($delete['status'] == 'success') {
        //     $response = [
        //         'status' => 'success',
        //         'message' => 'Attribute has been deleted.'
        //     ];
        // }
        // else {
        //     $response = [
        //         'status' => 'failed',
        //         'message' => $delete['message']['Errors']
        //     ];
        // }
        $response = [
            'status' => 'failed',
            'message' => ['Delete Feature is currently disabled for security purposes. ']
        ];
        return response()->json($response);
    }
}

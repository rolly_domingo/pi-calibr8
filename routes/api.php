<?php
header("Access-Control-Allow-Origin: *");
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'API\AuthController@login');
Route::get('/test', function() {
	return bcrypt('Rolly123!');
});
Route::get('/asset-servers', 'API\PIController@getAssetServers');
Route::get('/asset/databases/{webid}', 'API\PIController@getAssetDatabases');
Route::get('/asset/elements/{webid}', 'API\PIController@getAssetElements');
Route::get('/asset/child-elements/{webid}', 'API\PIController@getChildElements');
Route::get('/asset/attributes/{webid}', 'API\PIController@getAttributes');
Route::post('/asset/parent-element/add', 'API\PIController@addParentElement');
Route::post('/asset/child-element/add', 'API\PIController@addChildElement');
Route::get('/asset-servers/unitclasses/{webid}', 'API\PIController@getUOM');
Route::get('/asset-servers/unitclasses/{webid}/units', 'API\PIController@getChildUOM');
Route::get('/assets/attributes/categories/{webid}', 'API\PIController@getAttributeCategories');
Route::post('/asset/attribute/create/{webid}', 'API\PIController@createAttribute');
Route::get('/asset/attribute/delete/{webid}','API\PIController@deleteAttribute');
Route::get('/asset/element/delete/{webid}', 'API\PIController@deleteElement');